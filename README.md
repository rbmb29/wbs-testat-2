# Vorbereitung auf das Testat #2 für WBS

Zuerst erstellt man ein Projekt auf GitLab (Create blank project) und clont dieses.

Als Beispiel, zum Initialisieren, wird diese Abfolge angegeben:
```
git clone git@git.thm.de:<Kürzel>/<Name>.git
cd <Name>
git switch -c main
touch README.md
git add README.md
git commit -m "add README"
git push -u origin main
```

**Falls bereits eine README existiert**, so reicht die folgende Abfolge aus:
```
git clone git@git.thm.de:<Kürzel>/<Name>.git
cd <Name>
```

Danach beginnt der eigentliche Teil.
Es wird empfohlen es in der hier erwähnten Abfolge abzuarbeiten.
Die Datei die für dieses Testat noch angepasst werden muss, ist die `style.css` Datei im Ordner `public`.

## Einbinden von GitLab Pages

1. Download der Zen Garden Dateien:
   - Auf http://www.csszengarden.com/
   - HTML File & CSS File Download
   - In den Ordner `/public` ablegen
2. .gitlab-ci.yml und Pipeline aufsetzen
   - Im Projekt unter `Settings > General` bei `Visibility, project features, permissions`: (Issues, Merge-Requests und) CI/CD aktivieren
   - CI / CD einstellen
     - Danach unter `Settings > CI/CD` bei `Runners` den Punkt `Enable shared runners for this project` aktivieren
   - .gitlab-ci.yml wie hier erstellen: https://howto.git-pages.thm.de/#verwendung-von-gitlab-pages
     - Branch muss eventuell von master auf main geändert werden
   - committen & pushen
3. Gitlab-pages aufrufen
   - unter `Settings > Pages` ist der Link, wenn alles geklappt hat!
   - Man muss selbst keine eigene Domain anlegen, dies passiert automatisch.

## Upload auf Heroku

1. Erstellen eines Accounts bei Heroku
2. Erstellen des php Projekts (Ähnliche Benutzerführung unter [Php Setup](https://gist.github.com/wh1tney/2ad13aa5fbdd83f6a489) bzw. [Heroku Php Setup](https://devcenter.heroku.com/articles/getting-started-with-php))
   1. Heroku CLI installieren: https://devcenter.heroku.com/articles/getting-started-with-php#set-up
   2. In Heroku einloggen mit `heroku login`
   3. Erstellen der Datei `composer.json` mit folgendem Inhalt:

          {}

   5. Erstellen der Datei `index.php` mit folgendem Inhalt:

          <?php
          header("Location: /public");
          ?>

   8. Heroku-Projekt erstellen mit `heroku create css-zen-garden-rb --region eu` (rb sind meine Initialen für Ruben Bimberg), sie müssen diese **für sich anpassen**
   9. Änderungen committen
   10. Pushen zu Heroku mit `git push heroku main`
3. App einsehen mit `heroku open`

Änderungen können jederzeit mit `git push heroku main` auch neu auf Heroku hochgeladen und aktualisiert werden.
